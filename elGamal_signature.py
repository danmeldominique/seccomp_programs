import math

parameters = {
    "g" : 0,
    "x" : 0,
    "p" : 0,
    "y" : 0,
    "k" : 0,
    "r" : 0,
    "s" : 0,
    "v" : 0,
    "w" : 0,
    "m" : 0,
}

def elgamal_signature_scheme(g,x,p,m):
    parameters['g'] = g
    parameters['m'] = m
    parameters['x'] = x
    parameters['p'] = p

    ##################
    # Key generation #
    ##################

    # Public Key

    # 1. Calculate y

    calculate_y(g,x,p)
    print('Public Key (g, p, y): ({},{},{})'.format(parameters['g'],parameters['p'],parameters['y']))

    ##################
    #    Signing     #
    ##################
    # 2. Select k value such that 1 >= k <= p -1 and gcd(k,p-1)

    select_k(p)
    print('k value is {}'.format(parameters['k']))
    # 3. Calculate r value

    calculate_r(g,parameters['k'],p)
    print('r value is {}'.format(parameters['r']))

    # 4. Calculate s

    calculate_s(parameters['k'],m,x,parameters['r'],p)

    print('Signed message (m, r, s): ({},{},{})'.format(parameters['m'],parameters['r'],parameters['s']))


    ##################
    #  Verification  #
    ##################

    # 5. R value is verified to see if signature is valid

    if (verify_r(parameters['r'],parameters['p']) == True):
        print('{} is a valid r value. It has been accepted.'.format(parameters['r']))
    else:
        print('{} is not a valid r value. It has been rejected.'.format(parameters['r']))

    # 6. If r is verfied, calculate v

    calculate_v(g,m,p)
    print('v value is {}'.format(parameters['v']))
    
    #Calculate w

    calculate_w (parameters['y'],parameters['r'],parameters['s'],parameters['p'])
    print('w value is {}'.format(parameters['w']))
    
    # 7. Verify if v and w values match. If so, the message has been sent, recieved and verified succesfully.

    if(parameters['w'] == parameters['v']):
        print('The message has been sent, recieved and verified succesfully.')
    else:
        print('ERROR: The message has not been sent, recieved and verified succesfully.')

    diagnostics()

def calculate_y(g,x,p):
    y = pow(g,x) % p
    parameters['y'] = y

def select_k(p):
    k = 0
    for i in range (10,1000):
        if((i >= 1 & i <= (p-1)) & math.gcd(i,(p-1))):
            k = i
            break
    parameters['k'] = k

def calculate_r(g,k,p):
    r = pow(g,k) % p
    parameters['r'] = r

def calculate_s(k, m, x, r, p):
    #s = k^-1 * (m-x*r) mod (p-1)
    calc_one = (m-x*r) % (p-1)
    calc_two = calc_one % (p-1)
    calc_three = inverseMod(11,(p-1))
    parameters['s'] = (calc_two * calc_three) % (p-1)

def verify_r(r,p):
    if (r >= 1 & r <= (p-1)):
        return True
    else:
        return False

def calculate_v(g, m, p):
    v = pow(g,m) % p
    parameters['v'] = v

def calculate_w(y,r,s,p):
    w = (pow(y,r) * pow(r,s)) % p
    parameters['w'] = w

def inverseMod(a, m):
    for i in range(1,m):
        if ( m*i + 1) % a == 0:
            return ( m*i + 1) // a
    return None

def diagnostics():
    message="All Values"
    print('\n' + message)
    for i in range(0,len(message)):
        print('-',end='')
    print('\n')
    for k,v in parameters.items():
        print('{}: {}'.format(k,v))



#elgamal_gen_sign_verify(g,x,p,m)  
elgamal_signature_scheme(2849,53,8081,37)

