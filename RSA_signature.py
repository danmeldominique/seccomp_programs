import math

parameters = {
    "m" : 0,
    "q" : 0,
    "p" : 0,
    "n" : 0,
    "e" : 0,
    "d" : 0,
    "s" : 0,
    "phi_n" : 0,
    "m_prime" : 0
}

def rsa_signature_scheme(m,p,q,e):
    parameters['q'] = q
    parameters['m'] = m
    parameters['e'] = e
    parameters['p'] = p

    ##################
    # Key generation #
    ##################

    # 1. Calculate n

    calculate_n(parameters['p'],parameters['q'])

    # 2. Calculate Phi n

    calculate_phi_n(parameters['p'],parameters['q'])

    # 3. Give public key
    print('Public key (n, e): ({},{})'.format(parameters['n'],parameters['e']))

   # PRIVATE KEY

    # 4. Calculate d

    calculate_d(parameters['e'],parameters['phi_n'])

    ##################
    #     Signing    #
    ##################

    # 5. Calculate s
    calculate_s(parameters['m'],parameters['d'],parameters['n'])


    ##################
    #  Verification  #
    ##################

    # 6. Calculate m prime

    calculate_m_prime(parameters['s'],parameters['e'],parameters['n'])

    # 7. Check if m matches m prime. If they are the same, message has been succesfully sent and verified.

    if(verify_message(parameters['m'], parameters['m_prime']) == True):
        print("The message has been successfully verified")
    else:
        print("The message has not been successfully verified")

    diagnostics()

def calculate_n(p,q):
    n = p * q
    parameters['n'] = n

def calculate_phi_n(p,q):
    phi_n = (p-1)*(q-1)
    parameters['phi_n'] = phi_n

def calculate_d(e,phi_n):
    d = inverseMod(e,phi_n)
    parameters['d'] = d

def calculate_s(m,d,n):
    parameters['s'] = pow(m,d) % n
    
def calculate_m_prime(s,e,n):
    parameters['m_prime'] = pow(s,e,n)

def verify_message(m,m_prime):
    if(m == m_prime):
        return True
    else:
        return False

def inverseMod(a, m):
    for i in range(1,m):
        if ( m*i + 1) % a == 0:
            return ( m*i + 1) // a
    return None

def diagnostics():
    message = "All Values"
    print('\n' + message)
    for i in range(0,len(message)):
        print('-',end='')
    print('\n')
    for k,v in parameters.items():
        print('{}: {}'.format(k,v))


#rsa_signature_scheme(m,p,q,e)
rsa_signature_scheme(123456,5563,3821,9623)